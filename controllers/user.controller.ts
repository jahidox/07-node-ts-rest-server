import { Request, Response } from "express-serve-static-core";
import User from "../models/user.model";

export const getUsers = async (req: Request, res: Response) => {
  const users = await User.findAll();
  res.json({
    users
  });
}

export const getUser = async (req: Request, res: Response) => {
  const {id} = req.params;
  const user = await User.findByPk(id);

  if(user){
    res.json({
      user
    });
  }else{
    res.status(404).json({
      msg: `No existe un usuario con el id ${id}`
    });
  }
}

export const postUser = async (req: Request, res: Response) => {
  const {body} = req;
  try {
    const email_exist = await User.findOne({
      where: {
        email: body.email,
      }
    });
    if(email_exist){
      return res.status(400).json({
        msg: 'Ya existe un usuario con ese email'
      });
    }
    // const user = await User.create(body);
    const user = User.build(body);
    await user.save();
    res.json({
      user
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({
      msg: 'Hable con el administrador'
    });
  }
}

export const putUser = async (req: Request, res: Response) => {
  const {id} = req.params;
  const {body} = req;
  try {
    const user = await User.findByPk(id);
    if(!user){
      res.status(404).json({
        msg: `No existe un usuario con el id ${id}`
      });
    }
    await user.update(body);
    res.json({
      user
    })
  } catch (error) {
    console.log(error)
    res.status(500).json({
      msg: 'Hable con el administrador'
    });
  }
}

export const deleteUser = async (req: Request, res: Response) => {
  const {id} = req.params;
  const user = await User.findByPk(id);
  if(!user){
    res.status(404).json({
      msg: `No existe un usuario con el id ${id}`
    });
  }
  // await user.destroy();
  await user.update({state: false});
  res.json({
    user
  })
}