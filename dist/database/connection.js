"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sequelize_1 = require("sequelize");
var db_connection = new sequelize_1.Sequelize('node-ts-rest-server', 'root', 'mysql-admin', {
    host: 'localhost',
    dialect: 'mysql',
    // logging: false, //Para no ver sql en consola
});
exports.default = db_connection;
//# sourceMappingURL=connection.js.map