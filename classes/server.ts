import express, {Application} from 'express';
import userRoutes from '../routes/user.route';
import cors from 'cors';
import db_connection from '../database/connection';

class Server {
  private app: Application;
  private port: string;
  private paths = {
    users: '/api/users'
  };

  constructor() {
    this.app = express();
    this.port = process.env.PORT || '8080';

    this.connect_db();
    this.middlewares();
    this.routes()
  }

  async connect_db() {
    try {
      await db_connection.authenticate();
      console.log('Connection has been established successfully.');
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }
  }

  middlewares() {
    this.app.use(cors());
    this.app.use(express.json()); //Body reading and parsing
    this.app.use( express.static('public'));
  }

  routes() {
    this.app.use(this.paths.users, userRoutes);
  }

  start() {
    this.app.listen(this.port, () => {
      console.log('Server running on port: ', this.port);
    });
  }
}

export default Server;