import {Sequelize} from 'sequelize';

const db_connection = new Sequelize('node-ts-rest-server', 'root', 'mysql-admin', {
  host: 'localhost',
  dialect: 'mysql',
  // logging: false, //Para no ver sql en consola
});

export default db_connection;